function ejercicio07(dia){
    var obj={};
    var dia_anterior, dia_siquiente;
    var dias=["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado"];
    for(var i=0;i<dias.length;i++){
        if(dia==dias[i]){
            obj.dia_actual=dias[i];
            obj.dia_anterior=dias[i-1];
            obj.dia_siquiente=dias[i+1];
        }
    }
    return obj;
}